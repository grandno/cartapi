<?php 

// CartAPI TESZT APP 
// Grandpiere Noel 2018.02.06-07 

class Rest {
	
	private $httpVersion = "HTTP/1.1";

	public function setHttpHeaders($contentType, $statusCode){
		
		$statusMessage = $this -> getHttpStatusMessage($statusCode);
		
		header($this->httpVersion. " ". $statusCode ." ". $statusMessage);		
		header("Content-Type:". $contentType);
	}
	
	public function getHttpStatusMessage($statusCode){
		$httpStatus = array(
			100 => "Folytatás",
			101 => "Kapcsolási protokollok",
			200 => "OK",
			201 => "létrehozva",
			202 => "Elfogadva",
			203 => "Nem hiteles információk",
			204 => "Nincs tartalom",
			205 => "Tartalom visszaállítása",
			206 => "Részleges tartalom",
			300 => "Többszörös választás",
			301 => "Állandóan mozgatva",
			302 => "Hiba!",
			303 => "...Egyéb",
			304 => "Nincs módosítva",
			305 => "Proxy használata",
			306 => "(fel nem használt)",
			307 => "Ideiglenes átirányítás",
			400 => "rossz kérés",
			401 => "Jogosulatlan",
			402 => "Fizetés szükséges",
			403 => "tiltott",
			404 => "Nem található",
			405 => "Mód nem engedélyezett",
			406 => "Nem elfogadható",
			407 => "Proxy hitelesítés szükséges",
			408 => "Request Timeout",
			409 => "Konfliktus",
			410 => "Elment",
			411 => "Hossza szükséges",
			412 => "Előfeltétel sikertelen",
			413 => "Entity túl nagy kérés",
			414 => "Request-URI túl hosszú",
			415 => 'Nem támogatott médiatípus',
			416 => "Keresett tartomány nem megfelelő",
			417 => "Az elvárás sikertelen",
			500 => "Belső kiszolgálói hiba",
			501 => "Nem hajtották végre",
			502 => "Rossz átjáró",
			503 => "Szolgáltatás nem elérhető",
			504 => "Gateway Timeout",
			505 => "HTTP verzió nem támogatott");

		return ($httpStatus[$statusCode]) ? $httpStatus[$statusCode] : $status[500];
	}
}
?>